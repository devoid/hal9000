Bill Of Materials
=======================

Materials
-----------

| Element                                        | Quantity |
|------------------------------------------------|----------|
| Raspberry Pi                                   |1         |
| Zealot S5 Speaker                              |1         |
| 100mm Game LED Button or WC-DC58N              |1         |
| Metalic Spray Paint                            |1         |
| PCV Plastic Foam A3 5mm                        |1         |
| Smart USB Power Adapter 6 port                 |1         |
| USB Microphone or Logitech C310 or HP HD-2300  |1         |
| mini-jack mini-jack cable                      |1         |
| mini usb cable                                 |2         |
