HAL9000 Project
===============

Introduction
------------

Project is based on well known fictional supercomputer from Stanley Kubrick movie **2001: Space Odyssey**.

It is still work in progress.

Schematics
----------

For now I have front project (dxf), and sticker projects (svg).

There is also `resources` directory - here are all additional informations and resources I have found about similar projects.

Building
--------

Requirements:
* large game button 100mm
* pcv or plywood for front panel
* spray paint (light silver metalic, dark metalic)
